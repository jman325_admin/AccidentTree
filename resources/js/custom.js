/**
 * Created by 黄健 on 2015-05-27.
 * 公共脚本，所有用户自定义的脚本都必须写在这里
 */

//如果需要另外引入其他JS文件，请再此处引入
//$.getScript("");

/**
 * 保存树数据
 */
function saveTreeData(){
    if(treeData.length == 0){
        alert("没有绘制事故树，请先绘制");
        return false;
    }

    //在此编写保存数据至数据库的相关代码

}

/**
 * 保存为图片
 */
function saveToImg(){
    if(treeData.length == 0){
        alert("没有绘制事故树，请先绘制");
        return false;
    }


    /*html2canvas($accidentTreeCanvas,
     {
     useCORS: true,
     allowTaint: true,
     letterRendering: true,
     onrendered: function(canvas)
     {
     var url = canvas.toDataURL();
     var triggerDownload = $("<a>").attr("href", url).attr("download", "accidentTree.png").appendTo("body");
     triggerDownload[0].click();
     triggerDownload.remove();
     //$accidentTreeCanvas.append(canvas);
     }
     });*/

}
