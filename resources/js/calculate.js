/**
 * Created by 黄健 on 2015-07-01.
 */

/**
 * 构建新的树数据用于计算
 * @returns {*} 返回新的数据串字符串
 */
/*function buildNewTreeData(){
    if(treeData.length == 0){
        alert("传入参数中无节点数据构建，请传入正确的数据格式");
    }else{
        var newTreeData = [];

        us.each(treeData, function(item, i){
            if(item.type != treeNodeTypeObj.YM && item.type != treeNodeTypeObj.HM){
                var newNode = {
                    "key":item.key,
                    "name":item.name,
                    "child":[],
                    "relation":"",
                    "event":[]
                };

                newTreeData.push(newNode);
            }
        });


        us.each(treeData, function(item, i){
            if(item.type == treeNodeTypeObj.YM || item.type == treeNodeTypeObj.HM){
                var key = item.key;
                var parentKey = item.parentKey;
                for(var j=0; j<newTreeData.length; j++){
                    if(newTreeData[j].key == parentKey){
                        switch (item.type){
                            case treeNodeTypeObj.YM:
                                newTreeData[j].relation = "and";
                                break;
                            case treeNodeTypeObj.HM:
                                newTreeData[j].relation = "or";
                                break;
                        }
                    }
                }
                for(var i=0; i<treeData.length; i++){
                    if(treeData[i].parentKey == key && treeData[i].type != treeNodeTypeObj.C){
                        var childNode = {
                            "key":treeData[i].key,
                            "name":treeData[i].name,
                            "child":[],
                            "relation":"",
                            "event":[]
                        };
                        for(var n=0; n<newTreeData.length; n++){
                            if(newTreeData[n].key == parentKey){
                                newTreeData[n].child.push(childNode);
                            }
                        }
                    }

                    if(treeData[i].parentKey == key && treeData[i].type == treeNodeTypeObj.C){
                        var childNode = {
                            "key":treeData[i].key,
                            "name":treeData[i].name,
                            "child":[],
                            "relation":"",
                            "event":[]
                        };
                        for(var n=0; n<newTreeData.length; n++){
                            if(newTreeData[n].key == parentKey){
                                newTreeData[n].event.push(childNode);
                            }
                        }
                    }
                }
            }
        });

        //console.log(newTreeData);
        console.log(JSON.stringify(newTreeData));

        return JSON.stringify(newTreeData);

    }

}*/

function geji() {
    if(checkTreeData('0')){
        var newTreeData = {id:"T",child:[], relation:"", event:[]};
        var tNode = findTreeNodesByType("T", treeData);
        buildNewTreeData(newTreeData, treeData, tNode[0].key);

        var result1 = cutsetCalculator.findCutset(newTreeData);
        var gejiData = cutsetCalculator.mergeCutset(result1);//处理生成割集
        var zuixiaogj = cutsetCalculator.mergeCutsetByPrime(gejiData);//处理生成最小割集
        //console.log(JSON.stringify(newTreeData));
        alert("割集计算结果：\n"+buildGejiStr(gejiData));
    }
}

/**
 * 递归生成新的数据结构，第一次传入的是顶上事件节点
 * @param _node 当前处理节点，传入的是新的数据对象{id:"",child:[],relation:"",event:[]}
 * @param _treeData 树数组
 * @param _key 本系统的数据结构中节点的key
 */
function buildNewTreeData(_node, _treeData, _key){
    if(findChilds(_key,_treeData).length == 0){//若没有与或门，则没有子节点，该子节点递归结束
        return ;
    }
    var _relation = findChilds(_key,_treeData)[0];
    if(_relation.type == 'YM'){
        _node.relation = 'and';
    }else if(_relation.type == 'HM'){
        _node.relation = 'or';
    }else{
        alert("#%#%#%%#%");
    }
    var _relationChilds = findChilds(_relation.key,_treeData);
    for(var i=0; i<_relationChilds.length; i++){
        var _nodeChild = {
            id: "",
            child: [],
            relation: "",
            event: []
        };
        if(_relationChilds[i].type != 'C'){
            _nodeChild.id = _relationChilds[i].number;
            buildNewTreeData(_nodeChild,_treeData,_relationChilds[i].key);
            _node.child.push(_nodeChild);
            continue;
        }else if(_relationChilds[i].type == 'C'){
            _nodeChild.id = _relationChilds[i].number;
            _node.event.push(_nodeChild);
            continue;
        }else{
            alert("#%#%#%%#%");
        }
    }
}

/**
 * 构建割集结果字符串
 * @param resultData
 * @returns {string}
 * 返回格式：
 * {x,x,x}
 * {x,x,x}
 */
function buildGejiStr(resultData) {
    var txt = "";
    for (var i = 0; i < resultData.length; i++) {

        for (var j = 0; j < resultData[i].length; j++) {
            if(j == 0) {
                txt += "{";
            }

            if(j == (resultData[i].length-1))
                txt += resultData[i][j].id;
            else txt += resultData[i][j].id+",";

            if(j == (resultData[i].length-1)){
                txt += "}";
            }
        }

        txt += "\n";
    }
    return txt;
}

/*function calculateGeji(){
    var newTreeDataStr = buildNewTreeData();

    if(newTreeDataStr){
        *//*$.getJSON("http://172.16.16.144/WebApplication2/Cutset.aspx?callback=?",{data:newTreeDataStr+""},function(result){
            alert(result);
        });*//*
        $.ajax({
            type: "POST", //访问WebService使用Post方式请求
            contentType: "application/json;charset=utf-8", //WebService 会返回Json类型
            url: "http://172.16.16.144/WebApplication2/Cutset.aspx?callback=?", //调用WebService
            data: "data=[{'key':'028c4d8a-6ba1-456f-8c86-508a3494f278','name':'wef','child':[{'key':'192a7a93-f59e-4d41-8127-94d685838034','name':'wefwegw','child':[],'relation':'','event':[]},{'key':'bf9bf427-391e-4d1a-9c23-555659476b00','name':'eee','child':[],'relation':'','event':[]}],'relation':'and','event':[]},{'key':'192a7a93-f59e-4d41-8127-94d685838034','name':'wefwegw','child':[{'key':'cde8e9cd-98c8-4241-aa4c-e29aa56bca23','name':'eee','child':[],'relation':'','event':[]},{'key':'21d46bc6-cc1a-4417-8d33-7199ed5eac20','name':'reg','child':[],'relation':'','event':[]}],'relation':'or','event':[]},{'key':'bf9bf427-391e-4d1a-9c23-555659476b00','name':'eee','child':[{'key':'fba52318-a1ae-434b-80e2-de0430b90f88','name':'wgewrge','child':[],'relation':'','event':[]},{'key':'110d1f9f-4ab3-43c7-825a-c7aa197b05bb','name':'trthrth','child':[],'relation':'','event':[]}],'relation':'or','event':[]},{'key':'cde8e9cd-98c8-4241-aa4c-e29aa56bca23','name':'eee','child':[],'relation':'','event':[]},{'key':'21d46bc6-cc1a-4417-8d33-7199ed5eac20','name':'reg','child':[],'relation':'','event':[]},{'key':'fba52318-a1ae-434b-80e2-de0430b90f88','name':'wgewrge','child':[],'relation':'','event':[]},{'key':'110d1f9f-4ab3-43c7-825a-c7aa197b05bb','name':'trthrth','child':[],'relation':'','event':[]}]", //Email参数
            dataType: 'json',
            beforeSend: function(x) { x.setRequestHeader("Content-Type", "application/json; charset=utf-8"); },
            error: function(x, e) { },
            success: function(response) { //回调函数，result，返回值
                alert(response);
            }
        });
    }
}*/


